import React, { Component } from "react";
import axios from "axios";
import _ from "lodash";
import "./vashi.css";
// import "./App.css";
import Grid from "@material-ui/core/Grid";
// images
import def from "../images/def.png";
import catalogpdf from "./images/01.pdf";
import VashiLogo3 from "./images/Vashi-logo-3.png";
import manojPic from "./images/manojPic.png";
import rightHeader1 from "./images/rightHeader1.png";
import rightHeader2 from "./images/rightHeader2.png";
import phoneImg from "./images/phoneImg.png";
import emailImg from "./images/emailImg.png";
import internetImg from "./images/internetImg.png";
import callIcon from "./images/callIcon.png";
import messageImg from "./images/messageImg.png";
import whatsappIcon from "./images/whatsappIcon.png";
import emailIcon from "./images/emailIcon.png";
import locationIcon1 from "./images/locationIcon1.png";
import QRCodeImg from "./images/QRCodeImg.png";
import socialFBIcon from "./images/socialFBIcon.png";
import socialInstagramIcon from "./images/socialInstagramIcon.png";
import socialTwitterIcon from "./images/socialTwitterIcon.png";
import socialYoutubeIcon from "./images/socialYoutubeIcon.png";
import socialLinkedInIcon from "./images/socialLinkedInIcon.png";
import shareIconCatalog from "./images/shareIconCatalog.png";
import shareIconGallery from "./images/Update.gif";
import shareIconOffices from "./images/shareIconOffices.png";
import shareIconShare from "./images/shareIconShare.png";
import locationIcon from "./images/locationIcon.png";
import { InlineShareButtons } from "sharethis-reactjs";
import { Button, Paper, Dialog, DialogTitle } from "@material-ui/core";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import { Link, Route, Switch, withRouter } from "react-router-dom";
class vCardCMP extends Component {
  state = {
    dp: null,
    name: "",
    designation: "",
    mobile: null,
    phone: null,
    email: null,
    website: null,
    vcard: null,
    ecard: null,
    facebook: null,
    linkedin: null,
    instagram: null,
    twitter: null,
    youtube: null,
    shareDailog: false,
    photoIndex: 0,
    isGalleryOpen: false,
    images: [],
    catalog: "#",
  };
  componentDidMount() {
    // console.log("this.props", this.props.match.params.id);

    const uid = this.props.match.params.id;
    const key = "bdccc56e-5bc9-4c65-a15e-c7f2d62b886d";
    axios
      .get(
        "https://api.procontact.app/api/Cards/UserCardData?userID=" +
          uid +
          "&templateID=15&api_key=" +
          key
      )
      .then(
        (response) => {
          console.log("API CALL", response.data);
          const user = response.data.ProUser;
          const company = response.data.Company;
          let dp = user.UserImageURL;
          let name = user.FullName;
          let designation = user.Designation;
          designation +=
            user.Department !== null && user.Department !== ""
              ? ", " + user.Department
              : "";
          let mobile = user.MobileNumber;
          let phone = user.PhoneNumber1;
          let email = user.Email;
          let website = company.Website;
          let message = response.data.UserSharingMessage;
          let vcard = response.data.VCardURL;
          let ecard = response.data.eCardURL;
          let facebook = company.FaceBook;
          let linkedin = company.LinkedInPage;
          let instagram = company.InstaPage;
          let twitter = company.Twitter;
          let youtube = null;
          let address = company.AddressLine1;
          let images = [];
          let catalog = "#";
          if (
            typeof response.data.ProUser.CustomFields !== "undefined" &&
            response.data.ProUser.CustomFields !== null
          ) {
            images = _.find(response.data.ProUser.CustomFields, (f) => {
              return f.CustomFieldInfo.FieldName == "Updates";
            });
            images = typeof images !== "undefined" ? images.FieldValue : [];
            catalog = _.find(response.data.ProUser.CustomFields, (f) => {
              return f.CustomFieldInfo.FieldName == "Catalog";
            });
            catalog =
              typeof catalog !== "undefined" ? catalog.FieldValue[0] : "#";
          }

          //
          console.log("images", images, catalog);
          address +=
            company.AddressLine2 !== null && company.AddressLine2 !== ""
              ? ", " + company.AddressLine2
              : "";
          address +=
            company.CityName !== null && company.CityName !== ""
              ? ", " + company.CityName
              : "";
          address +=
            company.ZipCode !== null && company.ZipCode !== ""
              ? " - " + company.ZipCode
              : "";
          address +=
            company.RegionName !== null && company.RegionName !== ""
              ? ", " + company.RegionName
              : "";
          address +=
            company.CountryName !== null && company.CountryName !== ""
              ? ", " + company.CountryName
              : "";
          //   console.log(
          //     "dp",
          //     dp,
          //     "AddressLine1: " + company.AddressLine1 + "| \n",
          //     "AddressLine2: " + company.AddressLine2 + "| \n",
          //     "CityName: " + company.CityName + "| \n",
          //     "ZipCode: " + company.ZipCode + "| \n",
          //     "RegionName: " + company.RegionName + "| \n",
          //     "CountryName: " + company.CountryName + "| \n"
          //   );
          this.setState((state) => {
            return {
              ...state,
              dp,
              name,
              designation,
              mobile,
              phone,
              email,
              website,
              vcard,
              ecard,
              facebook,
              linkedin,
              instagram,
              twitter,
              youtube,
              address,
              message,
              images,
              catalog,
            };
          });
        },
        (error) => {
          console.log({ ...error });
          this.props.history.push("/");
        }
      );
  }
  shareDailogClose = () => {
    this.setState((state) => {
      return {
        ...state,
        shareDailog: false,
      };
    });
  };
  shareDailogOpen = () => {
    this.setState((state) => {
      return {
        ...state,
        shareDailog: true,
      };
    });
  };
  render() {
    const state = this.state;
    const images = this.state.images;
    return (
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        style={{
          color: "#000",
          height: "100%",
          width: "100%",
          // marginLeft: "30vw",
          margin: "0",
          border: "1px solid #000",
        }}
        className="main-content"
        spacing={4}
      >
        {/* Header */}
        <Grid item className="banner" style={{ width: "100%", padding: 0 }}>
          <Grid
            container
            direction="column"
            justify="flex-end"
            alignItems="flex-end"
          >
            <Grid
              item
              className="bannerStrip"
              // style={{
              //   width: "98%",
              //   background: "#00aff0",
              //   height: "150px",
              //   paddingTop: 30,
              //   borderBottomLeftRadius: "100px",
              // }}
            >
              <Grid
                container
                direction="row"
                justify="flex-end"
                alignItems="flex-start"
              >
                <Grid item xs="4" sm="4" md="4" lg="4">
                  <div className="mainLogo">
                    <img src={VashiLogo3} alt="VashiLogo3" />
                  </div>
                </Grid>
                <Grid item xs="4" sm="4" md="4" lg="4">
                  <div className="profileImg">
                    {this.state.dp == "" || this.state.dp == null ? (
                      <img src={def} alt="profilePic" />
                    ) : (
                      <img src={this.state.dp} alt="profilePic" />
                    )}
                  </div>
                </Grid>
                <Grid item xs="4" sm="4" md="4" lg="4">
                  <Paper
                    style={{
                      padding: "0 5px",
                      textAlign: "center",
                      width: "max-content",
                      margin: "0 auto",
                    }}
                  >
                    <ul className="bannerUL">
                      <li>
                        <img src={rightHeader1} alt="rightHeader1" />
                      </li>
                      <li>
                        <img src={rightHeader2} alt="rightHeader2" />
                      </li>
                    </ul>
                  </Paper>
                </Grid>
              </Grid>
            </Grid>
            <Grid
              item
              className="bannerName"
              style={{ width: "100%", textAlign: "center" }}
            >
              <p
                style={{
                  margin: 5,
                  fontSize: "2em",
                  color: "#00aff0",
                  fontWeight: "bold",
                }}
              >
                {state.name}
              </p>
            </Grid>
            <Grid
              item
              style={{ marginBottom: 10, width: "100%", textAlign: "center" }}
            >
              <p style={{ margin: 0 }}>{state.designation}</p>
            </Grid>
          </Grid>
        </Grid>
        {/* Contact Row 1 */}
        <Grid
          item
          className="contactDetailsBlock"
          style={{ flex: 1, width: "100%" }}
        >
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
            style={{
              color: "#000",
              height: "100%",
              width: "calc(100% - 20px)",
              marginTop: 20,
              marginLeft: 20,
            }}
            spacing={1}
          >
            {state.mobile == null || state.mobile == "" ? null : (
              <Grid item style={{ flex: 1, width: "100%" }}>
                <Grid
                  container
                  direction="row"
                  justify="flex-start"
                  alignItems="center"
                  style={{
                    color: "#000",
                    height: "100%",
                    width: "100%",
                  }}
                  spacing={3}
                >
                  <Grid item className="contactDetailsIcon">
                    <img src={phoneImg} alt="phoneImg" />
                  </Grid>
                  <Grid item>
                    <a href={"tel:" + state.mobile}>{state.mobile}</a>
                  </Grid>
                </Grid>
              </Grid>
            )}
            {state.phone == null || state.phone == "" ? null : (
              <Grid item style={{ flex: 1, width: "100%" }}>
                <Grid
                  container
                  direction="row"
                  justify="flex-start"
                  alignItems="center"
                  style={{
                    color: "#000",
                    height: "100%",
                    width: "100%",
                  }}
                  spacing={3}
                >
                  <Grid item className="contactDetailsIcon">
                    <img src={phoneImg} alt="phoneImg" />
                  </Grid>
                  <Grid item>
                    <a href={"tel:" + state.phone}>{state.phone}</a>
                  </Grid>
                </Grid>
              </Grid>
            )}
            <Grid item style={{ flex: 1, width: "100%" }}>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
                style={{
                  color: "#000",
                  height: "100%",
                  width: "100%",
                }}
                spacing={3}
              >
                <Grid item className="contactDetailsIcon">
                  <img src={emailImg} alt="emailImg" />
                </Grid>
                <Grid item>
                  <a href={"mailto:" + state.email}>{state.email}</a>
                </Grid>
              </Grid>
            </Grid>
            <Grid item style={{ flex: 1, width: "100%" }}>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
                style={{
                  color: "#000",
                  height: "100%",
                  width: "100%",
                }}
                spacing={3}
              >
                <Grid item className="contactDetailsIcon">
                  <img src={internetImg} alt="internetImg" />
                </Grid>
                <Grid item>
                  <a
                    href={state.website}
                    rel="noopener noreferrer"
                    target="_blank"
                  >
                    {state.website}
                  </a>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        {/* ICONS - COLOR  */}
        <Grid
          item
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
          }}
        >
          <Grid
            container
            direction="row"
            justify="space-evenly"
            alignItems="center"
            style={{
              color: "#000",
              height: "100%",
              width: "90%",
              margin: "0 auto",
            }}
          >
            <Grid item className="color-icons-1">
              <div>
                <a
                  rel="noopener noreferrer"
                  target="_blank"
                  href={"tel:" + state.mobile}
                >
                  <div className="icon">
                    <img src={callIcon} alt="callIcon" />
                  </div>
                  <div className="iconName">Call</div>
                </a>
              </div>
            </Grid>
            <Grid item className="color-icons-1">
              <div>
                <a
                  rel="noopener noreferrer"
                  target="_blank"
                  href={"tel:" + state.mobile}
                >
                  <div className="icon">
                    <img src={messageImg} alt="messageImg" />
                  </div>
                  <div className="iconName">Text</div>
                </a>
              </div>
            </Grid>
            <Grid item className="color-icons-1">
              <div>
                <a
                  href={
                    "https://wa.me/%2B91" +
                    state.mobile +
                    "?text=Hello%2C%20I%20Am%20interested%20in%20your%20products%2C%20please%20contact%20me%20when%20you%20are%20free%20!"
                  }
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <div className="icon">
                    <img src={whatsappIcon} alt="whatsappIcon" />
                  </div>
                  <div className="iconName">WhatsApp</div>
                </a>
              </div>
            </Grid>
            <Grid item className="color-icons-1">
              <div>
                <a
                  href={"mailto:" + state.email}
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <div className="icon">
                    <img src={emailIcon} alt="emailIcon" />
                  </div>
                  <div className="iconName">Email</div>
                </a>
              </div>
            </Grid>
            <Grid item className="color-icons-1">
              <div>
                <a
                  href="https://goo.gl/maps/HLfZydXXewhBVMTAA"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <div className="icon">
                    <img src={locationIcon1} alt="locationIcon1" />
                  </div>
                  <div className="iconName">Location</div>
                </a>
              </div>
            </Grid>
          </Grid>
        </Grid>
        {/* Save Contact / QR */}
        <Grid item>
          <div className="saveContactButton blue" download>
            <a href={state.vcard} rel="noopener noreferrer" target="_blank">
              Save Contact
            </a>
          </div>
        </Grid>
        <Grid item>
          <div className="QRCodeImg">
            <img src={QRCodeImg} alt="QRCodeImg" />
          </div>
        </Grid>
        {/* Icons B/W */}
        <Grid item>
          <Grid
            container
            direction="row"
            justify="space-evenly"
            alignItems="center"
            style={{
              color: "#000",
              height: "100%",
            }}
            spacing={2}
          >
            <Grid item>
              <a
                href={state.facebook}
                rel="noopener noreferrer"
                target="_blank"
              >
                <img src={socialFBIcon} alt="socialFBIcon" />
              </a>
            </Grid>
            <Grid item>
              <a
                href={state.instagram}
                rel="noopener noreferrer"
                target="_blank"
              >
                <img src={socialInstagramIcon} alt="socialInstagramIcon" />
              </a>
            </Grid>
            <Grid item>
              <a
                href={state.linkedin}
                rel="noopener noreferrer"
                target="_blank"
              >
                <img src={socialLinkedInIcon} alt="socialLinkedInIcon" />
              </a>
            </Grid>
            <Grid item>
              <a href={state.twitter} rel="noopener noreferrer" target="_blank">
                <img src={socialTwitterIcon} alt="socialTwitterIcon" />
              </a>
            </Grid>
            <Grid item>
              <a
                // href={state.youtube}
                href="https://www.youtube.com/user/vashielectricals"
                rel="noopener noreferrer"
                target="_blank"
              >
                <img src={socialYoutubeIcon} alt="socialYoutubeIcon" />
              </a>
            </Grid>
          </Grid>
        </Grid>
        {/* Module Icons Block */}
        <Grid item>
          <Grid
            container
            direction="row"
            justify="space-evenly"
            alignItems="center"
            style={{
              color: "#000",
              height: "100%",
            }}
            spacing={1}
          >
            <Grid item>
              <div className="shareIconParentList">
                <a href={state.catalog} target="_blank" download="Catalog.pdf">
                  <div className="iconImg">
                    <img src={shareIconCatalog} alt="shareIconCatalog" />
                  </div>
                  <div className="iconText">Catalog</div>
                </a>
              </div>
            </Grid>
            <Grid item>
              <div className="shareIconParentList">
                <a
                  href=" https://vashielectricals.com/pricelist/"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <div className="iconImg">
                    <img src={shareIconCatalog} alt="shareIconCatalog" />
                  </div>
                  <div className="iconText">Pricelist</div>
                </a>
              </div>
            </Grid>
            <Grid item>
              <div
                className="shareIconParentList"
                onClick={() =>
                  this.setState((state) => {
                    return {
                      ...state,
                      isGalleryOpen: true,
                    };
                  })
                }
              >
                <div className="iconImg" style={{ padding: 0,background: 'none',width: 71, height: 65, }}>
                  <img
                    src={shareIconGallery}
                    alt="shareIconGallery"
                    style={{ width: 71, height: 65,marginTop:2,marginBottom:-10,paddingBottom:0 }}
                  />
                </div>
                <div className="iconText">Updates</div>
              </div>
            </Grid>
            <Grid item>
              <div className="shareIconParentList">
                <a
                  href="https://vashielectricals.com/contactus/"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <div className="iconImg">
                    <img src={shareIconOffices} alt="shareIconOffices" />
                  </div>
                  <div className="iconText">Offices</div>
                </a>
              </div>
            </Grid>
            <Grid item>
              <div
                className="shareIconParentList"
                style={{ cursor: "pointer" }}
                onClick={this.shareDailogOpen}
              >
                <div className="iconImg">
                  <img src={shareIconShare} alt="shareIconShare" />
                </div>
                <div className="iconText">Share</div>
              </div>
            </Grid>
          </Grid>
        </Grid>
        {/* Footer */}
        <Grid item style={{ padding: 0, width: "100%" }}>
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            className="bottomAddres"
          >
            <Grid item style={{ width: "10%" }}>
              <div className="leftIcon">
                <img src={locationIcon} alt="locationIcon" />
              </div>
            </Grid>
            <Grid item style={{ width: "80%" }}>
              <div className="rightText">
                <p>{state.address}</p>
              </div>
            </Grid>
          </Grid>
        </Grid>
        <Dialog
          onClose={this.shareDailogClose}
          aria-labelledby="simple-dialog-title"
          open={state.shareDailog}
        >
          <DialogTitle
            id="simple-dialog-title"
            style={{ widht: "100%", textAlign: "center" }}
          >
            Share this Card
          </DialogTitle>
          <div style={{ padding: 20 }}>
            <InlineShareButtons
              config={{
                alignment: "center", // alignment of buttons (left, center, right)
                color: "social", // set the color of buttons (social, white)
                enabled: true, // show/hide buttons (true, false)
                font_size: 16, // font size for the buttons
                labels: "null", // button labels (cta, counts, null)
                language: "en", // which language to use (see LANGUAGES)
                networks: [
                  // which networks to include (see SHARING NETWORKS)
                  "whatsapp",
                  "email",
                  "linkedin",
                  "messenger",
                  "facebook",
                  "twitter",
                  "telegram",
                ],
                padding: 12, // padding within buttons (INTEGER)
                radius: 4, // the corner radius on each button (INTEGER)
                show_total: false,
                size: 40, // the size of each button (INTEGER)

                // OPTIONAL PARAMETERS
                url:
                  state.ecard == null || state.ecard == ""
                    ? state.website !== null || state.website !== ""
                      ? state.website
                      : "http://procontact.app"
                    : state.ecard, // (defaults to current url)
                image: "https://bit.ly/2CMhCMC", // (defaults to og:image or twitter:image)
                description: state.message, // (defaults to og:description or twitter:description)
                title: state.message, // (defaults to og:title or twitter:title)
                message: state.message, // (only for email sharing)
                subject: state.message, // (only for email sharing)
                username: state.message, // (only for twitter sharing)
              }}
            />
          </div>
        </Dialog>
        {state.isGalleryOpen && (
          <Lightbox
            mainSrc={images[state.photoIndex]}
            nextSrc={images[(state.photoIndex + 1) % images.length]}
            prevSrc={
              images[(state.photoIndex + images.length - 1) % images.length]
            }
            onCloseRequest={() =>
              this.setState((state) => {
                return {
                  ...state,
                  isGalleryOpen: false,
                };
              })
            }
            onMovePrevRequest={() => {
              this.setState((state) => {
                return {
                  ...state,
                  photoIndex:
                    (state.photoIndex + images.length - 1) % images.length,
                };
              });
            }}
            onMoveNextRequest={() => {
              this.setState((state) => {
                return {
                  ...state,
                  photoIndex: (state.photoIndex + 1) % images.length,
                };
              });
            }}
          />
        )}
      </Grid>
    );
  }
}

export default withRouter(vCardCMP);
