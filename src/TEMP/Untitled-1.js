import React from "react";

const Untitled1 = () => {
  return (
    <Grid container direction="column" justify="center" alignItems="center">
      <Grid
        item
        className="firstColumnItem"
        xs={12}
        sm={12}
        md={6}
        lg={6}
        xl={6}
      >
        <Grid container direction="column" justify="center" alignItems="center">
          <Grid
            item
            className="secondColumnItem"
            xs={12}
            sm={12}
            md={12}
            lg={12}
            xl={12}
          >
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Grid
                item
                xs={12}
                sm={12}
                md={12}
                lg={12}
                xl={12}
                className="firstColumnItem1stRow1stItem"
              >
                <div className="header">
                  <div className="leftHeader">
                    <div className="logo">
                      <img src={VashiLogo3} alt="Vashi-logo-3" />
                    </div>
                  </div>
                  <div className="middleHeader">
                    <div className="middleProfileImage">
                      <img src={manojPic} alt="manojPic.png" />
                    </div>
                  </div>
                  <div className="rightHeader">
                    {/* <div className="title">
                        <h1>CRISIL</h1>
                        <h4>
                          A <span> S&P Global Company</span>
                        </h4>
                        <h5>An "A-/Stable" Rated Company</h5>
                      </div> */}
                    <div className="rightHeaderTop">
                      <img src={rightHeader1} alt="rightHeader1" />
                    </div>
                    <div className="rightHeaderBottom">
                      <img src={rightHeader2} alt="rightHeader2" />
                    </div>
                  </div>
                </div>
              </Grid>
            </Grid>
          </Grid>

          <Grid
            item
            className="secondColumnItem2"
            xs={12}
            sm={12}
            md={12}
            lg={12}
            xl={12}
          >
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Grid
                item
                xs={12}
                sm={12}
                md={12}
                lg={12}
                xl={12}
                className="firstColumnItem1stRow2ndItem"
              >
                <div className="profileTile">
                  <h1 className="name">Manoj Shisale</h1>
                  <h3 className="details">Business Head, Solar Distribution</h3>
                </div>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      <Grid
        item
        className="firstColumnItem2"
        xs={12}
        sm={12}
        md={6}
        lg={6}
        xl={6}
      >
        <Grid container direction="row" justify="center" alignItems="center">
          <Grid
            item
            className="firstColumnItem2Row1stItem1st"
            xs={12}
            sm={12}
            md={12}
            lg={12}
            xl={12}
          >
            <div className="contact">
              <div className="contactParent">
                <div className="contactParentItemList">
                  <div className="contactDetailsimage1">
                    <img src={phoneImg} alt="phoneImg" />
                  </div>
                  <div className="contactDetails">
                    <a href="tel:+91 82918 45867">+91 82918 45867</a>
                  </div>
                </div>
                <div className="contactParentItemList">
                  <div className="contactDetailsimage1">
                    <img src={phoneImg} alt="phoneImg" />
                  </div>
                  <div className="contactDetails">
                    <a href="tel:+91 2522 663781">+91 2522 663781</a>
                  </div>
                </div>
                <div className="contactParentItemList">
                  <div className="contactDetailsimage1">
                    <img src={emailImg} alt="emailImg" />
                  </div>
                  <div className="contactDetails">
                    <a href="mailto:manoj.sh@vashielectricals.com">
                      manoj.sh@vashielectricals.com
                    </a>
                  </div>
                </div>

                <div className="contactParentItemList">
                  <div className="contactDetailsimage1">
                    <img src={internetImg} alt="internetImg" />
                  </div>
                  <div className="contactDetails">
                    <a href="https://vashielectricals.com/" target="_blank">
                      www.vashielectricals.com
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </Grid>
        </Grid>

        <Grid
          container
          direction="row"
          justify="flex-start"
          alignItems="center"
        >
          <Grid
            item
            className="firstColumnItem2Row2ndItem1st"
            xs={12}
            sm={12}
            md={12}
            lg={12}
            xl={12}
          >
            <div className="contact2">
              <div className="contact2Parent2">
                <div className="contact2Parent2ItemList">
                  <div className="icon">
                    <a href="tel:+91 82918 45867" target="_blank">
                      <img src={callIcon} alt="callIcon" />
                    </a>
                  </div>
                  <div className="iconName">
                    <a href="tel:+91 82918 45867" target="_blank">
                      Call
                    </a>
                  </div>
                </div>

                <div className="contact2Parent2ItemList">
                  <div className="icon">
                    <a href="javascript:void(0);" target="_blank">
                      <img src={messageImg} alt="messageImg" />
                    </a>
                  </div>
                  <div className="iconName">
                    <a href="javascript:void(0);" target="_blank">
                      Text
                    </a>
                  </div>
                </div>

                <div className="contact2Parent2ItemList">
                  <div className="icon">
                    <a
                      href="https://wa.me/%2B918291845867?text=Hello%2C%20I%20Am%20interested%20in%20your%20products%2C%20please%20contact%20me%20when%20you%20are%20free%20!"
                      target="_blank"
                    >
                      <img src={whatsappIcon} alt="whatsappIcon" />
                    </a>
                  </div>
                  <div className="iconName">
                    <a
                      href="https://wa.me/%2B918291845867?text=Hello%2C%20I%20Am%20interested%20in%20your%20products%2C%20please%20contact%20me%20when%20you%20are%20free%20!"
                      target="_blank"
                    >
                      WhatsApp
                    </a>
                  </div>
                </div>

                <div className="contact2Parent2ItemList">
                  <div className="icon">
                    <a
                      href="mailto:manoj.sh@vashielectricals.com"
                      target="_blank"
                    >
                      <img src={emailIcon} alt="emailIcon" />
                    </a>
                  </div>
                  <div className="iconName">
                    <a
                      href="mailto:manoj.sh@vashielectricals.com"
                      target="_blank"
                    >
                      Email
                    </a>
                  </div>
                </div>

                <div className="contact2Parent2ItemList">
                  <div className="icon">
                    <a
                      href="https://goo.gl/maps/HLfZydXXewhBVMTAA"
                      target="_blank"
                    >
                      <img src={locationIcon1} alt="locationIcon1" />
                    </a>
                  </div>
                  <div className="iconName">
                    <a
                      href="https://goo.gl/maps/HLfZydXXewhBVMTAA"
                      target="_blank"
                    >
                      Location
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </Grid>
        </Grid>

        <Grid container direction="row" justify="center" alignItems="center">
          <Grid
            item
            className="firstColumnItem2Row3rdItem1st"
            xs={12}
            sm={12}
            md={12}
            lg={12}
            xl={12}
          >
            <div className="contact3">
              <div className="saveContactButton">
                <a href="#">Save Contact</a>
              </div>
              <div className="QRCodeImg">
                <img src={QRCodeImg} alt="QRCodeImg" />
              </div>
              <div className="socialIconParent">
                <div className="socialIconParentList">
                  <a
                    href="https://www.facebook.com/vashielectricals/"
                    target="_blank"
                  >
                    <img src={socialFBIcon} alt="socialFBIcon" />
                  </a>
                </div>
                <div className="socialIconParentList">
                  <a
                    href="https://www.instagram.com/vashielectricals/"
                    target="_blank"
                  >
                    <img src={socialInstagramIcon} alt="socialInstagramIcon" />
                  </a>
                </div>
                <div className="socialIconParentList">
                  <a
                    href="https://www.linkedin.com/company/vashielectricals/?originalSubdomain=in"
                    target="_blank"
                  >
                    <img src={socialLinkedInIcon} alt="socialLinkedInIcon" />
                  </a>
                </div>
                <div className="socialIconParentList">
                  <a href="https://twitter.com/vashielectrical" target="_blank">
                    <img src={socialTwitterIcon} alt="socialTwitterIcon" />
                  </a>
                </div>
                <div className="socialIconParentList">
                  <a
                    href="https://www.youtube.com/user/vashielectricals"
                    target="_blank"
                  >
                    <img src={socialYoutubeIcon} alt="socialYoutubeIcon" />
                  </a>
                </div>
              </div>

              <div className="shareIconParent">
                <div className="shareIconParentList">
                  <div className="iconImg">
                    <button id="shareIconCatalog">
                      <img src={shareIconCatalog} alt="shareIconCatalog" />
                    </button>
                  </div>
                  <div className="iconText">Catalog</div>
                </div>
                <div className="shareIconParentList">
                  <a
                    href=" https://vashielectricals.com/pricelist/"
                    target="_blank"
                  >
                    <div className="iconImg">
                      <button id="shareIconCatalog">
                        <img src={shareIconCatalog} alt="shareIconCatalog" />
                      </button>
                    </div>
                    <div className="iconText">Pricelist</div>
                  </a>
                </div>

                <div className="shareIconParentList">
                  <div className="iconImg">
                    <button id="shareIconGallary">
                      <img src={shareIconGallary} alt="shareIconGallary" />
                    </button>
                  </div>
                  <div className="iconText">Updates</div>
                </div>
                <div className="shareIconParentList">
                  <a
                    href="https://vashielectricals.com/contactus/"
                    target="_blank"
                  >
                    <div className="iconImg">
                      <button id="shareIconOffices">
                        <img src={shareIconOffices} alt="shareIconOffices" />
                      </button>
                    </div>
                    <div className="iconText">Offices</div>
                  </a>
                </div>
                <div className="shareIconParentList">
                  <div className="iconImg">
                    <button id="shareIconShare">
                      <img src={shareIconShare} alt="shareIconShare" />
                    </button>
                  </div>
                  <div className="iconText">Share</div>
                </div>
              </div>
            </div>
          </Grid>
        </Grid>
      </Grid>

      <Grid
        item
        className="firstColumnItem3"
        xs={12}
        sm={12}
        md={6}
        lg={6}
        xl={6}
      >
        <Grid container direction="column" justify="center" alignItems="center">
          <Grid
            item
            className="firstColumnItem3Inner1stColumnItem"
            xs={12}
            sm={12}
            md={12}
            lg={12}
            xl={12}
          >
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Grid
                item
                xs={12}
                sm={12}
                md={12}
                lg={12}
                xl={12}
                className="firstColumnItem3Inner1stColumnItem1stRowItem"
              >
                <div className="bottomAddres">
                  <div className="leftIcon">
                    <img src={locationIcon} alt="locationIcon" />
                  </div>
                  <div className="rightText">
                    <p>
                      A-6, Plot No. 74, Shree Ganesh Complex, Behind Gupta
                      Compound, Dapode Road, Mankoli Naka, Bhiwandi, Thane –
                      421305
                    </p>
                  </div>
                </div>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Untitled1;
