import React from "react";

const Contact1Item = (props) => {
  const { image, text } = props;
  return (
    <div className="contactParentItemList">
      <div className="image">
        <img src={image} alt="image" />
      </div>
      <div className="contactDetails">
        <a href="tel:+91 82918 45867">+91 82918 45867</a>
      </div>
    </div>
  );
};

export default Contact1Item;
