import React, { Component } from "react";
import axios from "axios";
import vCardCMP from "./vashi/vCard";
// import "./App.css";
import Grid from "@material-ui/core/Grid";
import VashiLogo3 from "./images/Vashi-logo-3.png";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
// BrowserRouter as Router,
class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route
            path="/"
            exact
            component={() => (
              <Grid
                container
                direction="column"
                alignItems="center"
                justify="center"
                style={{ height: "100vh" }}
              >
                <Grid item>
                  <h1>Card Not Found</h1>
                </Grid>
                <Grid item>
                  <img src={VashiLogo3} alt="VashiLogo3" />
                </Grid>
              </Grid>
            )}
          />
          <Route
            path="/vashi"
            exact
            component={() => (
              <Grid
                container
                direction="column"
                alignItems="center"
                justify="center"
                style={{ height: "100vh" }}
              >
                <Grid item>
                  <h1>Unknown User</h1>
                </Grid>
                <Grid item>
                  <img src={VashiLogo3} alt="VashiLogo3" />
                </Grid>
              </Grid>
            )}
          />
          <Route path="/vashi/:id?" component={vCardCMP} />
          {/* <Route path="/Vashi/:id?" component={vCardCMP} /> */}
        </Switch>
      </Router>
    );
  }
}

export default App;
